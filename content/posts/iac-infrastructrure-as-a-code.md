---
title: "Iac Infrastructrure as a Code"
date: 2020-02-19T09:09:51+03:00
draft: true
---

|                ![Change](/img/iac-infrastructrure-as-a-code/chain.jpg)                |
| :-----------------------------------------------------------------------------------: |
| _Cloud provider bağımlılığı büyüyen projelerde bir risk olarak görülebilir mi?_ <br/> |

AWS yada GCP yada Azure üzerinde kaynakların oluşturulması ve durumlarının yönetilmesi büyük projeler için kontrolü git gide zorlaşan bir hal alabilir.
