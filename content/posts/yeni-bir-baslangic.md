---
title: "Yeni Bir Başlangıç"
date: 2020-02-16T10:18:11+03:00
draft: false
comments: true
tags: ["diger"]
---

|                                                   ![Change](/img/yeni-bir-baslangic/vantagepoint.jpg)                                                    |
| :------------------------------------------------------------------------------------------------------------------------------------------------------: |
| _Olaylara baktığınız yer aldığınız sonuçları doğrudan etkiler_ <br/> <a target="_blank" href="https://www.imdb.com/title/tt0443274/">_Vantage Point_</a> |

Yaklaşık 10 yıldır Microsoft .net framework, sharepoint ve .net core tabanlı uygulamalar geliştiren birisi olarak son yıllarda kullandığım ve geliştirme ortamlarını ve yaklaşımlarını beğendiğim aynı zamanda daha iyi topluluk destekleri olan teknolojilere geçmek istediğime karar verdim. .net core ve bağlı geliştirmeleri yapmaya bir müddet daha devam edecek olsamda orta vadede bu değişikliği tamamlamış olmak ve yeni ortamlarda geliştirmelerimi yapmaya devam etmek istiyorum.

## Nereden başladım

Developer olarak ilk işimi 3. sınıfta aldım. Okulun bir şirketinde part-time olarak çalışmaya başladım.

Kullandığımız teknolojiler

1. C# 3.5
2. NHibernate (yanılmıyorsam 2.x sürümleri)
3. MSSQL 2008
4. Telerik (windows ve web componentleri)
5. FTP (yazılımları güncellemek müşterilerde bulunan client uygulamalarımız ftp üzerinden sync işlerini yapıyordu, upgrade-downgrade)
6. jQuery
7. Asp.Net Web Forms

2011 yılında SharePoint ile tanıştım. ilk çalıştığım proje turkcell.com.tr olmuştu. Sonrasında birçok kurumsal firmanın web siteleri ve intranetlerini .net ve SharePoint ile geliştirme işlerinde bulundum.

İki yüksek lisans yaptım, ilkinde (**_Bilgi Teknolojileri_**) 4, ikincisinde (**_Mühendislik Yönetimi_**) 8 ders verip bıraktım. Eğer af gelirse sondan geriye doğru tamamlamak isterim : )

---

## Nereye gitmek istiyorum

2020 yılı itibari ile yeni alanlara yelken açmak üzere bu blogu açıyorum. Hedefim Cloud native yaklaşımı başta olmak üzere, konfor alanım dışında işler yapmak, yeni teknolojiler öğrenmek ve bu yolda yaşadığım sorunları ve kazanımları bu blogta aktarmak.

Sevgiler\
Sadri

- Blog sistemini oluştururken Seçuk Cihan'ın <a href="https://blog.selcukcihan.com/web-development/bloglari-birlestir/" target="_blank">şu yazısından</a> yardım aldım
