---
title: "Multi-thread uygulamalar ve thread-safe mekanızmalar C# ve GO"
date: 2020-02-16T10:18:11+03:00
draft: false
comments: true
tags: ["yazilim", "Go"]
---

| ![Change](/img/multi-thread-lock-durumlari/conflict.jpg) |
| :------------------------------------------------------: |
|              _İki thread-bir kaynak_ <br/>               |

Uygulama performansını arttırmak amaçlı multi thread uygulamalar geliştirdiğimizde ne gibi durumlar ortaya çıkıyor?

Geçtiğimiz hafta iki uygulama arasında var olan bir entegrasyon uygulamasının bilginin asıl kaynağından gelen kayıtları farklı sıralarda DB'ye eklediği için sorun yaşandığını öğrendim.

Yazılım ekibi ile yaptığımız toplantıda .net framework 4.6 ile geliştirilen uygulama için multi-thread bir yapı ile işlemleri yaptıklarını ve thread içinde de parallel kütüphanesi ile kayıtların işlendiğini öğrendik.

Uygulamayı hızlandırmak için yapılan geliştirmeler bir anlamda başka bir soruna yol açmış.

Öğrenme hedefi ile çıktığım bu yolda ben de C# ve GO ile buradaki problemi thread safe bir mekanızma ile nasıl çözeriz bakmak istiyorum.

**Öncelikle farklı iş parçacıklarının ortak memory alanlarına aynı anda erişmesini istemiyoruz.**

## Lock

Lock kullandığımız kod bloğunu ancak ve ancak bir iş parçacığının aynı anda çalıştırmasını garanti eder.

Peki bunu nasıl başarıyor?

Mutex objesi mutlaka referans tipi bir obje olmalıdır
objeler RAM'de tutulurken bizim bildiğimiz SIZE ile tutulmazlar, CLR ayrıca RTTI (Runtime Type Information) ve Object Header olmak üzere iki alanda da referans tipine ait bazı bilgileri saklar.
lock mekanızması Object Header içinde tutulur. CLR burada erişimi engelleyecek bir durum olursa heap için ayrıca lock objelerinin tutulduğu bir alan oluşturur .
Bundan dolayıdır ki value type bir değişkeni lock yapamıyoruz.

{{< highlight cs "" >}}
lock(mutex) {
// DB Access veya başka bir işlem
}
{{< / highlight >}}

Kullanım detaylarını geçiyorum ve aynı işlemi GO ile yapmak istediğimde neleri kullanmam gerekiyor, inceliyorum.

## İyi bir Google araması

Öğrenme aşamasında kullandığım tekniklerin başında eğer bir teknojinin adını biliyorsam **abc vs** diye yazıp bekliyorum. Google bana bununla ilgili yapılmış karşılatırmaları veriyor ve buradaki referansları takip ederek aradığımı buluyorum.

Go için _go thread safe_ diye arama yaptığımda gelen sonuçlardan başlıyoruz.

[sync.Mutex](https://golang.org/pkg/sync/#Mutex) C# için kullandığımız lock mekanızmasına benzerliği ile karşımıza çıktı.

Lock ile benzer olmasına karşın GO bize başka bir alternatif olarak threadler arasında iletişim kurmamızı sağlayan channel mekanızmasını sunuyor.

Lock ve Channel için örnek birer uygulamayı sonraki yazıda ekleyeceğim.

Go öğrenmek için biraz karışık ilerleyeceğim. Parçaları sonradan birleştireceğiz:)
