---
title: "Nasıl pilot olurum?"
date: 2021-01-24
draft: false
---

## Pilotluk: Bir meslek olarak sana göre mi?

10 yıldır yazılım yapan biri olarak başka bir alanda uzmanlaşmak, tamamen yeni birşey öğrenmek son derece heyecanlıydı. Akışkanlar dinamiği, elektrik gibi (fizik dersi içinde aldım ama peh) dersleri daha önce almamış biri olarak bunların dahil olduğu koca bir sistemi incelemek ve öğrenmeye çalışmak son derece heyecanlı. Sonunda edindiğin bütün teorik bilgi ile bir uçağı uçurabilmenin kendisi de bu heyacan ve adrenalinin doruk noktası. Gel gelelim meslek olarak yapmak istediğinizden emin olmanız gerekiyor daha en başında. 

Bunun araştırmasını nasıl yaparım, pilotluk bana göre mi diye uzun uzun düşünmeniz ve pilotların nasıl bir 24-saat yaşam döngülerinin olduğunu iyi biliyor olmanız bu aşamada son derece önemli. 

Devam edelim ..

## Okul araştırması

Türkiye'de son yıllarda açılan okullarla beraber okul sayısı 20'ye yaklaştı (net sayısı bilmemekle beraber). Bana göre bir uçuş okulunun iyi olması 
- Uçakları ne kadar bakımlı
- Uçakları ne kadar yeni 
- Uçak bakımlarını kendileri mi yapıyor 
- Uçakların uçamama zamanları (ne kadar arızalı kalıyor uçaklar ortalama)
- Lokasyon (sana göre)
- Uçuş yapılan hava alanının coğrafik şartları (dağlık vs..)
- Uçuş yapılan hava alanının ortalama uçuşa elverişli gün sayısı 
- Öğrenci başına düşen uçak sayısı ort. 
- Eğitmenlerinin kalitesi 

## Tanıtım Uçuşu 

Hizmet almayı düşündüğünüz okul ile iletişime geçip 30dk tanıtım uçuşu isteyin mutlaka. Kokpitte uçmak, küçük bir uçakta uçuyor olmak size nasıl gelecek.

## PPL 

Private Pilot Licence pilotluk macerasının ilk sertifikasıdır. (sağlık serfikasını saymazsak:) SHGM tarafından onaylı bir okuldan yer derslerini tamamladıktan sonra (9 ders) 45 saat uçuş ve kontrol uçuşuyla sonlanır. Yaklaşık 2-3 ay (hava şartlarına bağlı olarak) sürer. Çalışanlar için süre daha da uzayabilir. 

## ATPL 

Airline Transfer Pilot Licence: Bir hava yolu aracını ticari amaçla kullanabilmek için gerekli olan lisans tipi. 14 dersten oluşur ve yer derslerini takiben bu 14 sınavı 75 ve üzerinde bir notla geçmeniz gerekir. 

*** Devam edecek... 