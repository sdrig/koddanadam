---
title: "C# Implicit ve Explicit Donusumler"
date: 2020-05-02T18:01:24+03:00
draft: false
---
# Giris
Program icinde kullandigimiz degiskenleri (value type yada reference type) farkli tipte degiskenlermis gibi kullanmak isteyebiliriz. Inheritance, polymorphism gibi OOP temellerine gore bu islemler cokca kullanacagimiz donusumler olacaktir. Biz burada primitive yani value tipli degiskenler icin donusumlere bakacagiz.

Ornek:

```csharp
int intVar = 33;
long longVar = intVar;
```
Compiler bu donusum icin harici bir islem gerektirmeden kucuk veriyi(int) buyuk veri yerin kullanir(long). Kucuk-buyuk ifade tiplerin MAX degerlerine gore ifade edilmistir. Aksi halde `longVar=3` ve `intVar=10` gibi bir aritmetic karsilastirma olarak karsimiza cikabilir herzaman. Bu donusumleri gosteren dokumani [bu dokumanda](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/numeric-conversions) detayli gorebilirsiniz.

`byte short int long float double` ornek bir implicit cast siralamasi. Elimde bir `byte` tipinde degisken varsa bunu saginda yer alan butun tiplere donusturebilirim, herhangi bir operator kullanmadan.

Bunun tersi durumlarda yani buyuk veri tipi kucuk veri tipine cast edilmek isteniyor ise mutlaka bir belirtec olmalidir.Bu da `explicit` yani belirgin casting olarak isimlendiriliyor.

```csharp
long longVar = Int64.MaxValue;
int intVar = (int)longVar;
```
Buyuk veriden kucuk veriye donusum yaparken data kaybini goz onunde bulundurmus ve bunu kabul ettigimizi varsayiyoruz.

C# icin hersey objedir diye bir kalip var ancak bunu su sekilde degistirmemiz gerekiyor. Hersey objeye cevrilebilir.
Ornegin `int` primitive bir degisken olup `object` ile kalitsal bir baglantisi yoktur ancak biz int tipini obje icine kutulayabiliriz (boxing).
Bu `int` bir obje oldugu icin degil, icinde `int` degeri saklayan yeni bir obje yaratabildigimiz icindir.

Boxing bir degiskeni objeye kutulamak iken,( yeni obje HEAP icinde saklanir. Bu da daha fazla memory tuketimi ve GC'nin daha cok calismasi anlamina geliyor. GC'nin ekstra calismasi CPU tuketimini arttirdigi gibi uygulamamizin respose suresini de arttirabilir. )
unboxing objeden olmasini istedigimiz tipe geri cevirmektir.

C# son surumleri birlikte `is` ve `as` operatorleri bu donusumler icin kullanabilecegimiz yeni yontemlerdir.

Bir degiskenin runtimeda hangi tiplere cevrilebilecegine `is` ile bakabiliriz.

[Dokumanda yer alan](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/operators/type-testing-and-cast) ornege bakalim.

```csharp
public class Animal { }

public class Giraffe : Animal { }

public static class TypeOfExample
{
  public static void Main()
  {
    object b = new Giraffe();
    Console.WriteLine(b is Animal);  // output: True
    Console.WriteLine(b.GetType() == typeof(Animal));  // output: False

    Console.WriteLine(b is Giraffe);  // output: True
    Console.WriteLine(b.GetType() == typeof(Giraffe));  // output: True
  }
}
```
Yukarda gordugumuz uzere `is` operatoru zurafa objesinin hem bir zurafa ayni zamanda da bir hayvan olabilecegini soyluyor.
Buna karsilik C#'in cok eski surumlerinden beri gelen `typeof` operatoru ise objenin o anki durumda hangi tipte oldugunu bize soyluyor.
Yani `is` operatoru bir casting oncesinde dogrulama icin kullanilirken `typeof` ise spresifik bir anda objenin tipini bize soyluyor.

`as` operatoru ise `(int)longVar` ifadesinin `longVar as int`
olarak da yazilabilmesini sagliyor. Gordugunuz gibi ikinci ifade daha okunabilir ve takip edilmesi daha kolay.

Sonraki yazilarda gorusmek uzere.

