---
title: "Binary Tree Traversal, Preorder, Inorder, Postorder "
date: 2020-04-07T10:18:11+03:00
draft: false
comments: true
tags: ["algorithm"]
---

# DFS ve BFS 
Ağaç veri yapısında gezinme algoritmaları olan DFS (Depth First Search) ve BFS(Breadth First Search)
için hangisi hangisiydi hatırlamak adına birkaç kolay bilgi vermek istiyorum. 

## BFS

Başka bir isimle __Level order Tree Travesal__ 

Bu yaklaşımda Root yada keyfi seçilecek bir elemandan bağlantılı olduğu bütün __sub node'ları__ ziyaret edeceğiz.

Level order, yani başladığımız yere göre öncelikli gelen ___satırlar___ önce ziyaret edilecek. 

## DFS 

DFS için üç farklı yöntem var. Öncelikle ön kabul olarak 

1. __left node her zaman right'tan daha önce ziyaret edilir__
2. Order ifadesi __Root nodun__ sıralamada nerede olduğuna bağlı olarak isimlendirmeler yapılmıştır. 

___Bu iki kurala göre aşağıdaki sıralamayı yapalım___
- Preorder - Pre  : Önce gelir   ->    Root - Left - Right
- Inorder  - In   : Ortada gelir ->    Left - Root - Right 
- Postorder- Post : Sonda gelir  ->    Left - Right- Root

Umarım aklınızda kalması için iyi bir yöntem olmuştur. 

Sevgiler

Faydalı Kaynaklar

> [GFG](https://www.geeksforgeeks.org/dfs-traversal-of-a-tree-using-recursion/) \
> [Video](https://www.youtube.com/watch?v=gm8DUJJhmY4&feature=emb_err_watch_on_yt) 