---
title: "Big O Notasyonu - Tanim ve Ornekler"
date: 2020-04-22T18:01:24+03:00
draft: false
---

> Big 0 natasyonu algoritmanin ne kadar hizli buyudugunu gostermek icin kullanilan bir gosterim seklidir.

### Tanim

[Time Complexity](https://en.wikipedia.org/wiki/Time_complexity) tanimi aslinda uygulamanin calisma suresini kesin olarak vermekten ziyade bir tahmini icerir. Bu sebeple bize kesin bir calisma suresi vermek yerine fonksiyonumuzun degisen girdilere gore nasil bir degisim gosterdiginin tahminini bize verir.

###### Ornegin bir FOR dongusunu ele alalim.

```go {linenos=table,hl_lines=[8,"15-17"],linenostart=199}
func forLoop(N int) {
    for var i:=0;i<N;i++{
        time.After(time.Second)
    }
}
test1: forLoop(1)
test2: forLoop(10)
test3: forLoop(100)
```

Yukarida ornekte gordugumuz `test1` `test2` ve `test3` verilen `N` degerine gore 1, 10 ve 100 saniye calisma surelerine sahip olacaklar. Bu `forLoop` fonksiyonumuzun N degerien gore lineer bir artis gosterdigini ve notasyonumuzun O(n) olacagini gosterir.

### Constant Time

Girdi degerinden bagimsiz olarak fonksiyonun calisma suresinin sabit bir deger olmasidir.

```go {linenos=table,hl_lines=[8,"15-17"],linenostart=199}
func forLoop(N int) {
    for i:=0;i<5;i++{
        time.Sleep(time.Second)
    }
}
test1: forLoop(1)
test2: forLoop(10)
test3: forLoop(100)
```

Ayni ornegi N degerini loop icin bir ust sinir olmaktan cikardigimizda ve 5 olarak degistirdigimizde fonksiyonumuz 1,10 ve 100 degerlerinden bagimsiz olarak 5sn olarak calisacaktir.

### Linear Time

Birinci forLoop orneginde bahsettigim gibi verilen N degerine gore fonksiyonumuz `N`e bagli olarak buyume gosteriyor.
`N*5, N/2, N + 1` gibi ifadeler de yine ayni sekilde `N`e bagli lineer artis gosterir.

### Quadratic time O(n2)

O(n) ile kiyasladigimizda cok daha hizli buyume gosterir.

```go {linenos=table,hl_lines=[8,"15-17"],linenostart=199}
func forLoop(N int) {
    for i:=0;i<N;i++{
         for j:=0;j<N;i++{
            time.Sleep(time.Second)
        }
    }
}
test1: forLoop(1)
test2: forLoop(10)
test3: forLoop(100)
```

| ![Quadratic Fonksiyon Benchmark](/img/big-o-notasyonu/big-o-quadratic.png) |

### Logaritmik time O(log(n))

```go {linenos=table,hl_lines=[8,"15-17"],linenostart=199}
func forLoop(N int) {
    for i:=0;i<N;i= i * 2 {
        time.Sleep(time.Second)
    }
}
test1: forLoop(1)
test2: forLoop(10)
test3: forLoop(100)
```

Yukarida `i` dongu degiskeni her bir donus icin onceki degerinin 2 katina cikarilmis. Bu da demek oluyorki `for` dongumuz log(N) olcusunde azalma gosterecektir.
[Logaritma Hesaplama Araci](https://www.rapidtables.com/calc/math/Log_Calculator.html)'ndan siz de `N` degerleri icin sonuclarinizi hesaplayabilirsiniz.

...

Bunlardan baska [Wikipedia](https://en.wikipedia.org/wiki/Time_complexity) sayfasindan diger tanimli notasyonlari okuyabilirsiniz.
