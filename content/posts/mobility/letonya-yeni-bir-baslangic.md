---
title: "Letonya'ya Taşındım!"
date: 2021-01-24
draft: false
---

> `“Change is a great and horrible thing, and people love it or hate it at the same time. Without change, however, you just don’t move.” -Marc Jacobs`

## 2020 ve götürdükleri
Geçen yıl yazılım alanından devops alanına geçiş yapmayı planlarken 2020 bize büyük sürprizlerle geldi ve bahanelerin ardına sığınarak da olsa planların gerçekleşmesini engelledi diyebilirim. 
Biraz daha geriye gidersek 2018 başlarında da farklı bir planla yola çıkmış ve sonunda geldiğim noktada pilotluk mesleğinin - meslek olarak- bana göre olmadığını anlamıştım. 
Pilotajı neden bıraktım konusunda bir video hazırlıyorum. Umarım orada daha detaylı anlatabilirim. 
2019 da biraz pilotaj biraz yazılım işlerine ağırlık verme olarak geçti ve 2020 de artık tamamen kendi alanımda çalışmaya karar verdim, yani yazılım yapmaya devam edecektim ve bu işten para kazanacaktım. 
Kısaca 2020'de hedeflerin tutmadığı bir yıl olarak karneme yazıldı ve hoşgeldin 2021. 

## 2021 ve neler oluyor?

Son 5 yıldır bireysel olarak danışmanlık işleri yapıyorum. Zaman zaman anahtar teslim projeler, zaman zaman full-time müşteri ofisinde çalışma şeklinde 5 yıl geçti. Kendi danışmanlık şirketi işime başlarken de hedefliğim nokta bu değildi. Bilahare bu konuda da birşeyler yazmaya çalışırım. 
Uzun lafın kısası danışmanlık alanında edindiğim tecrübeler şimdi Accenture Letonya'da orta düzey bir yönetici olarak çalışmaya başlıyorum. Neden Letonya, neden başka bir yer değil konusuna da 2023'te değinmek üzere notumu düşüyorum :) 

Girişteki sözün 1000 farklı varyasyonunu sürekli ve düzenli duymuşsunuzdur. Ben de buna inanan ve değişimin gelişmek için bir önşart olduğunu düşünen biriyim. 

Risk alma (yönetme) ve değişim isteği bizi ancak bir noktadan bir noktaya taşıyabilir. Başka türlüsü bana biraz zor geliyor. 

